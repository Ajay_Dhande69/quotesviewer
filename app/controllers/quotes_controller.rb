class QuotesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
  end

  def get_quotes
    render json: { status: 200, quote: Quote.all.order("RAND()").limit(1) }
  end

  def create_quote
    @quote = Quote.new(quote_params)
    if @quote.save
      render json: { status: 200, message: 'Quote saved successfully.' }
    else
      render json: { status: 302, message: 'Quote not saved.' }
    end
  end

  private

  def quote_params
    params.require(:quote).permit(:src_quote_id, :title, :content, :link)
  end
end
