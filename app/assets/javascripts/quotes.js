$(function () {

  // Get quotes from api
  function fetchQuotes() {
    src_url = "https://quotesondesign.com/";
    $.ajax({
      url: src_url + "wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1",
      success: function (t) {
        var result = t.shift();
        saveQuoteToDB(result);
      },
      cache: false
    });
  }

  // Send quotes to save
  function saveQuoteToDB(result) {
    $.ajax({
      type: "POST",
      url: "/create_quote",
      data: {
        quote: {
          src_quote_id: result.ID,
          title: result.title,
          content: result.content,
          link: result.link
        }
      },
      dataType: "json",
      success: function (response) {
        saveQuoteResponse(response);
      }
    });
  }

  // Handle the response from saveQuoteToDB()
  function saveQuoteResponse(response) {
    if (response.status == 200) {
      console.log(response.message);
    } else if (response.status == 302) {
      console.log(response.message);
    } else {
      console.log('Something goes wrong.');
    }
  }

  function showQuotes() {
    $.ajax({
      type: 'GET',
      url: '/get_quotes',
      data: '',
      dataType: "json",
      success: function (response) {
        if (response.status == 200) {
          $('#quote-container').empty();
          $('#quote-container').append(
            '<h1>' + response.quote[0].title + '</h1>' +
            '<p>' + response.quote[0].content + '</p>'
          ).fadeIn();
        } else {
          $('#quote-container').empty();
          $('#quote-container').append(
            '<h1>' + 'No Quotes Available' + '</h1>'
          ).fadeIn();
        }
      }
    });
  }

  // Show quote at initial state
  showQuotes();

  // show quote at every 10 seconds
  setInterval(function () {
    showQuotes();
  }, 10000);

  // Get quotes from api at every 10 seconds
  setInterval(function () {
    fetchQuotes();
  }, 10000);
});