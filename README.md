# README

Things you may want to cover:

* Ruby version : ruby-2.5.1
* Database : mysql

* Deployment instructions
```bash
  bundle install
```

* Database creation
```bash
  rails db:create
```
```bash
  rails db:migrate
```
```bash
  rails db:seed
```
* Launch the application
```bash
  rails s
```