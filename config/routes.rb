Rails.application.routes.draw do
  root 'quotes#index'
  post 'create_quote', to: 'quotes#create_quote'
  get 'get_quotes', to: 'quotes#get_quotes'
end
