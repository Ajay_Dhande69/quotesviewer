class CreateQuotes < ActiveRecord::Migration[5.2]
  def change
    create_table :quotes do |t|
      t.string :title
      t.text :content
      t.text :link
      t.timestamps
    end
  end
end
