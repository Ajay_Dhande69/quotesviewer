class AddSrcQuoteIdToQuotes < ActiveRecord::Migration[5.2]
  def change
    add_column :quotes, :src_quote_id, :integer, after: 'id'
    add_index :quotes, :src_quote_id, unique: true
  end
end
