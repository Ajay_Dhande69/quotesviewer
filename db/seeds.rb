# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Quote.create(src_quote_id: 54, title: 'Charles Eames', content: "<p>The details are not the details. They make the design.</p>", link: "https://quotesondesign.com/charles-eames/")
puts "1 Quote created."